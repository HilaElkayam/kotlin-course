package com.kotlin.learn

import kotlinx.coroutines.*
import org.junit.Test
import java.lang.Runnable
import kotlin.concurrent.thread

class `13ThreadsAndCoroutines` {


    @Test
    fun simpleThread() {

        class SimpleThread : Thread() {
            override fun run() {
                println(">> ${Thread.currentThread()} has run. <<")
            }
        }

        val thread = SimpleThread()
        thread.start()


        Thread.sleep(100)
    }

    @Test
    fun evenSimplerThread() {
        thread(start = true) {
            println(">> ${Thread.currentThread()} has run. <<")
        }
        Thread.sleep(100)
    }

    @Test
    fun multipleThreads() {
        var j: Int = 0
        repeat(20) {
            Thread(Runnable {
                println("${++j}: ${Thread.currentThread().name}")
            }).start()
        }
        Thread.sleep(100)
    }

    @Test
    fun simpleCoroutine() {
        GlobalScope.launch { // launch a new coroutine in background and continue
            delay(1000L) // non-blocking delay for 1 second (default time unit is ms)
            println("World!") // print after delay
        }
        println("Hello,") // main thread continues while coroutine is delayed
        Thread.sleep(2000L) // block main thread for 2 seconds to keep JVM alive
    }

    @Test
    fun coroutineJoin() {
        runBlocking {
            val job = launch { // launch a new coroutine and keep a reference to its Job
                delay(1000L)
                println("World!")
            }
            println("Hello,")
            job.join() // wait until child coroutine completes
        }
    }

    @Test
    fun weightyThreads() {
        repeat(200_000) {
            thread(start = true, isDaemon = false) {
                print(".")
            }
        }
    }

    @Test
    fun lightweightCoroutine() {
        runBlocking {
            repeat(200_000) { // launch a lot of coroutines
                launch {
                    print(".")
                }
            }
        }
    }

    @Test
    fun asyncIsAlsoAthing() {
        runBlocking {
            val lengthy_calculation_1 = async {
                delay(500L)
                7 //The actual result
            }
            val lengthy_calculation_2 = async {
                delay(500L)
                4 //The actual result
            }
            val res1 = lengthy_calculation_1.await()
            val res2 = lengthy_calculation_2.await()

            println(res1 + res2)
        }
    }

    @Test
    fun failedConcurrentSum() {

        runBlocking {
            val one = async<Int> {
                try {
                    delay(Long.MAX_VALUE) // Emulates very long computation
                    42
                } finally {
                    println("First child was cancelled")
                }
            }
            val two = async<Int> {
                println("Second child throws an exception")
                throw ArithmeticException()
            }
            one.await() + two.await()
        }
    }


    @Test
    fun showWhereCoroutinesRun() {
        var i: Int = 0
        repeat(20) {
            GlobalScope.launch {
                val threadName = Thread.currentThread().name
                println("${++i}: $threadName")
            }
        }
        Thread.sleep(100)
    }


    @Test
    fun showCoroutinesPickingUpOnAnotherThread() {
        repeat(20) {
            GlobalScope.launch {
                println("Before delay $it: ${Thread.currentThread().name}")
                delay(10)
                println("After delay $it: ${Thread.currentThread().name}")
            }
        }
        Thread.sleep(200)
    }


}