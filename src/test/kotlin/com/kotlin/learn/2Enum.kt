package com.kotlin.learn

import org.junit.Test

/**
 * Implement Card class which have two fields
 * shape which is enum of all cards shapes and the number of the card
 * Also create a Deck class which have a list of those 52 cards.
 * make the test work:

class TestIt{

    @Test
    fun printCards (){
        Deck()
        for (card in Deck.cards) {
            println(" card suit is ${card.shape} and card number is ${card.number}")
        }
    }
}
 */
