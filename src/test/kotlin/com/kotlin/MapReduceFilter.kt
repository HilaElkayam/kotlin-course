package com.kotlin

import org.assertj.core.api.Assertions.assertThat
import org.junit.Test
import kotlin.math.pow

class MapReduceFilter {

    @Test
    fun `using map to double each element actually works`() {
        val list = listOf(1, 2, 3, 4)
        val double = list.map { it * 2 }

        assertThat(double).isEqualTo(listOf(2, 4, 6, 8))

        println(double)
    }

    @Test
    fun `using map to square each element actually works`() {
        val list = listOf(1, 2, 3, 4)
        val double = list.map { n -> n.toDouble().pow(2) }

        assertThat(double).isEqualTo(listOf(1.0, 4.0, 9.0, 16.0))

        println(double)
    }

    @Test
    fun `let's see how filtering odd numbers work`() {
        val list = mutableListOf<Int>()
        (1..50).forEach { list.add(it) }

        val evensOnly = list.filter { it % 2 == 0 }

        (1..50).forEach { n ->
            if (n % 2 == 0) {
                assertThat(n).isIn(evensOnly)
            } else {
                assertThat(n).isNotIn(evensOnly)
            }
        }

        println(evensOnly)
    }

    @Test
    fun `now let's calculate a sum`() {
        val list = listOf(2, 3, 4)
        val sum = list.reduce { acc, i -> acc + i }
        assertThat(sum).isEqualTo(2 + 3 + 4)
        println(sum)
    }

    @Test
    fun `And here's a functional method for calculating factorials`() {
        val list = listOf(2, 3, 4)
        val factorial = list.fold(1) { acc, i -> acc * i }
        assertThat(factorial).isEqualTo(2 * 3 * 4)
        println(factorial)
    }

    @Test
    fun `How to reverse sort strings`() {
        val fruits = listOf("banana", "apple", "cherry", "coconut", "blueberry", "apricot")
        val sortedFruits = fruits.sortedBy { it.first() }

        assertThat(sortedFruits).isEqualTo(listOf("apple", "apricot", "banana", "blueberry", "cherry", "coconut"))

        assertThat(sortedFruits.first()).isEqualTo("apple")

        println(sortedFruits)

        // And reversed
        assertThat(sortedFruits.reversed().first()).isEqualTo("coconut")
        println(sortedFruits.reversed())
    }
}