package com.kotlin.answer
/*
regular
number = 1
number = 2
number = 3
number = 2
number = 4
number = 6
number = 3
number = 6
number = 9
sum = 36

i want that when it reaches i*j =4, it will stop
 */
fun main(args: Array<String>)  {

    var sum = 0
    var number: Int

    lit@ for (j in 1..3) {

        for (i in 1..3) {
            number = i*j
            println("number = $number")

            if (number == 4)
                break@lit

            sum += number

        }
    }
    print("sum = $sum")
}
