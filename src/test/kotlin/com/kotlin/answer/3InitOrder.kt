package com.kotlin.answer

/*
The class Movie is started below. An instance of class Movie represents a film. This class
has the following three class variables:
    ● title, which is a Stringrepresenting the title of the movie
    ● studio, which is a Stringrepresenting the studio that made the movie
    ● rating, which is a Stringrepresenting the rating of the movie (i.e. PG­13, R, etc)

    1. Write a constructor for the class Movie, which takes a String representing the title of the
    movie, a String representing the studio, and a String representing the rating as its
    arguments, and sets the respective class variables to these values.

    2. Write a second constructor for the class Movie, which takes a String representing the title
    of the movie and a String representing the studio as its arguments, and sets the respective
    class variables to these values, while the class variable rating is set to "PG"

    3. change the title to always be capitalized

 */

class Movie( _title: String,val studio:String, val rating:String ){
    val title:String

    init {
        title= _title.capitalize()
    }

    constructor( title: String, studio:String):this(title,studio, "PG" )
}