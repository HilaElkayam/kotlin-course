package com.kotlin.answer

interface BankAccount{
    val salary:Int
    val accountNumber:Int

    fun deposit(){
        println("deposit $salary in $accountNumber")
    }
}
open class Person(val age: Int) {
    fun eat() = println("knows how to eat")
    fun drink() = println("knows how to drink")
    open fun printDetails() = println("Age $age")
}

class MathTeacher(age: Int, val name:String): Person(age), BankAccount {
    override val salary: Int= 10000
    override val accountNumber: Int=223388

    fun teachMath()= println("knows how to teach math")
    override fun printDetails() = println("Age $age name $name")

}

class Businesswoman(age: Int): Person(age) {
    fun closeDeal()= println("knows how to close deal")
}
