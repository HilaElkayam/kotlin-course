package com.kotlin.answer

import com.kotlin.answer.Deck.Companion.cards
import org.junit.Test

data class Card(var number: Int, var suit: Suit)

enum class Suit {
    DIAMONDS, CLUBS, HEARTS, SPADES
}

class Deck {
    companion object {
        val cards = mutableListOf<Card>()
    }

    init {
        for (suit in Suit.values()) {
            for (i in 1..13) {
                cards.add(Card(i, suit))
            }
        }
    }
}


class TestIt{

    @Test
    fun printCards (){
        Deck()
        for (card in cards) {
            println(" card suit is ${card.suit} and card number is ${card.number}")
        }
    }
}

