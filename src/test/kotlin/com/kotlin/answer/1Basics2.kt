package com.kotlin.answer

/* we want to create a class bank account with:
* customer name - we get the full name, split it to first and last name
* account number
*
* Add a format function that prints : Customer Zohar Avisror has account number TS-12345
*
* */
class Basics {

    lateinit var firstName:String
    lateinit var lastName:String

    var customerFullName:String = ""
        set(value) {
            if (value.isNullOrEmpty()) throw IllegalArgumentException(
                "Name cannot be empty")
            field = value
            firstName = value.split(" ").first()
            lastName = value.split(" ").last()

        }

    lateinit var accountNumber:String

    fun format() = println("Customer $firstName $lastName has account number $accountNumber")

}