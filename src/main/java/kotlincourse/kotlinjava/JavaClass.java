package kotlincourse.kotlinjava;

import com.kotlincourse.kotlinjava.KotlinClass;

public class JavaClass {

    public String jfoo() {
        return "Called method jfoo";
    }

    public static void main(String[] args) {

        System.out.println(
                new KotlinClass()
                        .kfoo()
        );
    }

}
