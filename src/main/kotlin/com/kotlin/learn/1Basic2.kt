package com.kotlin.learn

class `Basics2`{

    val name: String
        get() {
            return "Jane Doe"
        }

    var age = 0
        set(value) {
            if (value < 0) throw IllegalArgumentException(
                "Age cannot be negative")
            field = value
        }

    init {
        this.age = age
    }
}