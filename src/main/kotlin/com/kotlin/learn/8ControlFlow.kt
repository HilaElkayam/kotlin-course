package com.kotlin.learn

import java.lang.Integer.parseInt

class `8ControlFlow` {

    fun ifFlow() {
        val a: Int = 0
        val b: Int = 1
        var max: Int
        if (a > b) {
            max = a
        } else {
            max = b
        }

        // As expression
        val max2 = if (a > b) a else b

        //last expression is the value of a block
        max = if (a > b) {
            print("Choose a")
            a
        } else {
            print("Choose b")
            b
        }
    }

    fun forFlow() {
        for (i in 1..3) { //range
            println(i)
        }
        for (i in 6 downTo 0 step 2) { //The Range .. can’t be used in the reverse order
            println(i)
        }

        val array = arrayListOf<Int>(1, 2, 3)
        for (i in array) {
            println(array[i])
        }

        (2..5).forEach {
            println(it)
        }

        (2..5).forEach { i ->
            println(i)
        }
    }

    fun continueOrBreak() {
        for (i in 1..10) {
            if (i == 5) {
                break
            }
            println(i) // will print 1 2 3 4
        }


        /* Any expression in Kotlin may be marked with a label
        A break qualified with a label jumps to the execution point right after the loop marked with that label.
        A continue proceeds to the next iteration of that loop.
        */
        loop@ for (i in 1..100) {
            for (j in 1..100) {
                if (j > 50) break@loop
            }
        }

        loop@ for (i in 1..100) {
            for (j in 1..100) {
                if (j > 50) continue@loop
            }
        }
    }

    fun returnWithLambda() {
        listOf(1, 2, 3, 4, 5).forEach {
            if (it == 3) return // non-local return directly to the caller of foo()
            print(it)
        }
        println("this point is unreachable")

        listOf(1, 2, 3, 4, 5).forEach lit@{
            if (it == 3) return@lit // local return to the caller of the lambda, i.e. the forEach loop
            print(it)
        }
        print(" done with explicit label")
    }

    fun whileFlow() {
        var x = 1
        while (x > 0) {
            x--
        }

        do {
            val y = retrieveData()
        } while (y != null) // y is visible here!
    }

    private fun retrieveData(): Int {
        return 2
    }


    //when matches its argument against all branches sequentially until some branch condition is satisfied
    fun whenFlow() {
        val x: Int = 5
        when (x) {
            1 -> print("x == 1")
            2 -> print("x == 2")
            else -> { // Note the block
                print("x is neither 1 nor 2")
            }
        }

        when (x) {
            parseInt("1") -> print("s encodes x")
            else -> print("s does not encode x")
        }

        val validNumbers = listOf<Int>(1, 2, 3, 4)
        when (x) {
            in 1..10 -> print("x is in the range")
            in validNumbers -> print("x is valid")
            !in 10..20 -> print("x is outside the range")
            else -> print("none of the above")
        }

        /*   var result = when(number) {
           0 -> "Invalid number"
           1, 2 -> "Number too low"
           3 -> "Number correct"
           in 4..10 -> "Number too high, but acceptable"
           !in 100..Int.MAX_VALUE -> "Number too high, but solvable"
           else -> "Number too high"
       }*/
    }
}
