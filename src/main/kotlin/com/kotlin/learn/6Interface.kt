package com.kotlin.learn

interface MyInterface {

    //interfaces can have properties but these properties need to be abstract.
    val test: Int

    // property with implementation
    val prop: Int
        get() = 23

    fun foo() : String  // abstract method

    fun hello() {
        println("Hello there, pal!")
    }
}

class InterfaceImp : MyInterface {

    //have to implement all abstract stuff - fields and functions
    override val test: Int = 25
    override fun foo() = "Lol"

    override fun hello() {
        println("Hello here, friend!")
    }
}

fun main(args: Array<String>) {
    val obj = InterfaceImp()

    println("test = ${obj.test}")
    print("Calling hello(): ")

    obj.hello()

    print("Calling and printing foo(): ")
    println(obj.foo())
}