package com.kotlincourse.kotlinjava

import kotlincourse.kotlinjava.JavaClass

class KotlinClass {

    fun kfoo(): String {
        return "called method kfoo()"
    }
}

fun main(args: Array<String>) {
    println(JavaClass().jfoo())
}
